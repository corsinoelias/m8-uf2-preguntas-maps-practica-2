package com.example.googlemapspractica2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.googlemapspractica2.model.Preguntas;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
private GoogleMap mMap;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    RadioGroup rgroup;
    RadioButton rbutton,radioButton1,radioButton2,radioButton3,radioButton4;
    double latitud,longitud;
    Button boton;
    private List<Preguntas> listPreguntas= new ArrayList<Preguntas>();
    TextView textView;
    int preguntaActual=1;
    int correctas=0;
    int idx=0;
static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS=23;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView= findViewById(R.id.textView);
        rgroup=findViewById(R.id.opciones);
        boton=findViewById(R.id.button);
        radioButton1=findViewById(R.id.radio1);
        radioButton2=findViewById(R.id.radio2);
        radioButton3=findViewById(R.id.radio3);
        radioButton4=findViewById(R.id.radio4);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preguntaActual!=1 && idx!=0){
                    rbutton.setChecked(false);
                }
                preguntaActual++;
                comprobarPregunta();
            }
        });
        checkPermission();
        FirebaseApp.initializeApp(this);
        SupportMapFragment supportMapFragment= (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference();
        getPreguntasFirebase();
    }

     void comprobarPregunta() {
        switch (preguntaActual){
            case 1:
                textView.setText(listPreguntas.get(0).getPregunta1());
                radioButton1.setText(listPreguntas.get(0).getRespuesta1a());
                radioButton2.setText(listPreguntas.get(0).getRespuesta1b());
                radioButton3.setText(listPreguntas.get(0).getRespuesta1c());
                radioButton4.setText(listPreguntas.get(0).getRespuesta1d());
                latitud=listPreguntas.get(0).getLatitud1();
                longitud=listPreguntas.get(0).getLogintud1();
                CameraUpdate camUpd1 = CameraUpdateFactory.newLatLngZoom(new LatLng(latitud,longitud), 3);
                mMap.animateCamera(camUpd1);
                break;
            case 2:
                textView.setText(listPreguntas.get(0).getPregunta2());
                radioButton1.setText(listPreguntas.get(0).getRespuesta2a());
                radioButton2.setText(listPreguntas.get(0).getRespuesta2b());
                radioButton3.setText(listPreguntas.get(0).getRespuesta2c());
                radioButton4.setText(listPreguntas.get(0).getRespuesta2d());
                latitud=listPreguntas.get(0).getLatitud2();
                longitud=listPreguntas.get(0).getLogintud2();
                CameraUpdate camUpd2 = CameraUpdateFactory.newLatLngZoom(new LatLng(latitud,longitud), 6);
                mMap.animateCamera(camUpd2);
                break;
            case 3:
                textView.setText(listPreguntas.get(0).getPregunta3());
                radioButton1.setText(listPreguntas.get(0).getRespuesta3a());
                radioButton2.setText(listPreguntas.get(0).getRespuesta3b());
                radioButton3.setText(listPreguntas.get(0).getRespuesta3c());
                radioButton4.setText(listPreguntas.get(0).getRespuesta3d());
                latitud=listPreguntas.get(0).getLatitud3();
                longitud=listPreguntas.get(0).getLogintud3();
                CameraUpdate camUpd3 = CameraUpdateFactory.newLatLngZoom(new LatLng(latitud,longitud), 3);
                mMap.animateCamera(camUpd3);
                break;
            case 4:
                textView.setText(listPreguntas.get(0).getPregunta4());
                radioButton1.setText(listPreguntas.get(0).getRespuesta4a());
                radioButton2.setText(listPreguntas.get(0).getRespuesta4b());
                radioButton3.setText(listPreguntas.get(0).getRespuesta4c());
                radioButton4.setText(listPreguntas.get(0).getRespuesta4d());
                latitud=listPreguntas.get(0).getLatitud4();
                longitud=listPreguntas.get(0).getLogintud4();
                CameraUpdate camUpd4 = CameraUpdateFactory.newLatLngZoom(new LatLng(latitud,longitud), 6);
                mMap.animateCamera(camUpd4);
                break;
            default:
                textView.setText("Test finalizado respuestas correcta "+correctas+"/4");
                radioButton1.setText("Respuesta correcta");
                radioButton2.setText("Respuesta incorrecta");
                radioButton3.setText("Respuesta incorrecta");
                radioButton4.setText("Respuesta incorrecta");
                Toast.makeText(getApplicationContext(),"El test ha terminado",Toast.LENGTH_SHORT).show();
        }
    }

    private void getPreguntasFirebase() {
        databaseReference.child("Preguntas").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listPreguntas.clear();
                for (DataSnapshot data: dataSnapshot.getChildren()) {
                    Preguntas p = data.getValue(Preguntas.class);
                    listPreguntas.add(p);
                }
                textView.setText(listPreguntas.get(0).getPregunta1());
                radioButton1.setText(listPreguntas.get(0).getRespuesta1a());
                radioButton2.setText(listPreguntas.get(0).getRespuesta1b());
                radioButton3.setText(listPreguntas.get(0).getRespuesta1c());
                radioButton4.setText(listPreguntas.get(0).getRespuesta1d());
                latitud=listPreguntas.get(0).getLatitud1();
                longitud=listPreguntas.get(0).getLogintud1();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[] {android.Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }else{
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                    mapFragment.getMapAsync(this);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        CameraUpdate camUpd1 = CameraUpdateFactory.newLatLngZoom(new LatLng(latitud,longitud), 3);
        mMap.animateCamera(camUpd1);
    }


    public void checkButton(View view) {
        int radioId= rgroup.getCheckedRadioButtonId();
        rbutton= findViewById(radioId);
        String[]opciones={"a","b","c","d"};
         idx = rgroup.indexOfChild(rbutton);
        switch (preguntaActual){
            case 1:
                if (listPreguntas.get(0).getCorrecta1().equals(opciones[idx])){
                    correctas++;
                }
            break;
            case 2:
                if (listPreguntas.get(0).getCorrecta2().equals(opciones[idx])){
                    correctas++;
                }
                break;
            case 3:
                if (listPreguntas.get(0).getCorrecta3().equals(opciones[idx])){
                    correctas++;
                }
                break;
            case 4:
                if (listPreguntas.get(0).getCorrecta4().equals(opciones[idx])){
                    correctas++;
                }
                break;

            default:
                Toast.makeText(getApplicationContext(),"El test ha terminado",Toast.LENGTH_SHORT).show();
        }
    }
}
