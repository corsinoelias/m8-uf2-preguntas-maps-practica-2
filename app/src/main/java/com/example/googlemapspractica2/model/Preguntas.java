package com.example.googlemapspractica2.model;

public class Preguntas {
    String pregunta1,pregunta2, pregunta3, pregunta4;
    String respuesta1a,respuesta1b,respuesta1c,respuesta1d;
    String respuesta2a,respuesta2b,respuesta2c,respuesta2d;
    String respuesta3a,respuesta3b,respuesta3c,respuesta3d;
    String respuesta4a,respuesta4b,respuesta4c,respuesta4d;
    String correcta1,correcta2,correcta3,correcta4;
    double latitud1,latitud2,latitud3,latitud4;
    double logintud1,logintud2,logintud3,logintud4;

    @Override
    public String toString() {
        return "Preguntas{" +
                "pregunta1='" + pregunta1 + '\'' +
                ", pregunta2='" + pregunta2 + '\'' +
                ", pregunta3='" + pregunta3 + '\'' +
                ", pregunta4='" + pregunta4 + '\'' +
                ", respuesta1a='" + respuesta1a + '\'' +
                ", respuesta1b='" + respuesta1b + '\'' +
                ", respuesta1c='" + respuesta1c + '\'' +
                ", respuesta1d='" + respuesta1d + '\'' +
                ", respuesta2a='" + respuesta2a + '\'' +
                ", respuesta2b='" + respuesta2b + '\'' +
                ", respuesta2c='" + respuesta2c + '\'' +
                ", respuesta2d='" + respuesta2d + '\'' +
                ", respuesta3a='" + respuesta3a + '\'' +
                ", respuesta3b='" + respuesta3b + '\'' +
                ", respuesta3c='" + respuesta3c + '\'' +
                ", respuesta3d='" + respuesta3d + '\'' +
                ", respuesta4a='" + respuesta4a + '\'' +
                ", respuesta4b='" + respuesta4b + '\'' +
                ", respuesta4c='" + respuesta4c + '\'' +
                ", respuesta4d='" + respuesta4d + '\'' +
                ", correcta1='" + correcta1 + '\'' +
                ", correcta2='" + correcta2 + '\'' +
                ", correcta3='" + correcta3 + '\'' +
                ", correcta4='" + correcta4 + '\'' +
                ", latitud1=" + latitud1 +
                ", latitud2=" + latitud2 +
                ", latitud3=" + latitud3 +
                ", latitud4=" + latitud4 +
                ", logintud1=" + logintud1 +
                ", logintud2=" + logintud2 +
                ", logintud3=" + logintud3 +
                ", logintud4=" + logintud4 +
                '}';
    }

    public String getPregunta1() {
        return pregunta1;
    }

    public void setPregunta1(String pregunta1) {
        this.pregunta1 = pregunta1;
    }

    public String getPregunta2() {
        return pregunta2;
    }

    public void setPregunta2(String pregunta2) {
        this.pregunta2 = pregunta2;
    }

    public String getPregunta3() {
        return pregunta3;
    }

    public void setPregunta3(String pregunta3) {
        this.pregunta3 = pregunta3;
    }

    public String getPregunta4() {
        return pregunta4;
    }

    public void setPregunta4(String pregunta4) {
        this.pregunta4 = pregunta4;
    }

    public String getRespuesta1a() {
        return respuesta1a;
    }

    public void setRespuesta1a(String respuesta1a) {
        this.respuesta1a = respuesta1a;
    }

    public String getRespuesta1b() {
        return respuesta1b;
    }

    public void setRespuesta1b(String respuesta1b) {
        this.respuesta1b = respuesta1b;
    }

    public String getRespuesta1c() {
        return respuesta1c;
    }

    public void setRespuesta1c(String respuesta1c) {
        this.respuesta1c = respuesta1c;
    }

    public String getRespuesta1d() {
        return respuesta1d;
    }

    public void setRespuesta1d(String respuesta1d) {
        this.respuesta1d = respuesta1d;
    }

    public String getRespuesta2a() {
        return respuesta2a;
    }

    public void setRespuesta2a(String respuesta2a) {
        this.respuesta2a = respuesta2a;
    }

    public String getRespuesta2b() {
        return respuesta2b;
    }

    public void setRespuesta2b(String respuesta2b) {
        this.respuesta2b = respuesta2b;
    }

    public String getRespuesta2c() {
        return respuesta2c;
    }

    public void setRespuesta2c(String respuesta2c) {
        this.respuesta2c = respuesta2c;
    }

    public String getRespuesta2d() {
        return respuesta2d;
    }

    public void setRespuesta2d(String respuesta2d) {
        this.respuesta2d = respuesta2d;
    }

    public String getRespuesta3a() {
        return respuesta3a;
    }

    public void setRespuesta3a(String respuesta3a) {
        this.respuesta3a = respuesta3a;
    }

    public String getRespuesta3b() {
        return respuesta3b;
    }

    public void setRespuesta3b(String respuesta3b) {
        this.respuesta3b = respuesta3b;
    }

    public String getRespuesta3c() {
        return respuesta3c;
    }

    public void setRespuesta3c(String respuesta3c) {
        this.respuesta3c = respuesta3c;
    }

    public String getRespuesta3d() {
        return respuesta3d;
    }

    public void setRespuesta3d(String respuesta3d) {
        this.respuesta3d = respuesta3d;
    }

    public String getRespuesta4a() {
        return respuesta4a;
    }

    public void setRespuesta4a(String respuesta4a) {
        this.respuesta4a = respuesta4a;
    }

    public String getRespuesta4b() {
        return respuesta4b;
    }

    public void setRespuesta4b(String respuesta4b) {
        this.respuesta4b = respuesta4b;
    }

    public String getRespuesta4c() {
        return respuesta4c;
    }

    public void setRespuesta4c(String respuesta4c) {
        this.respuesta4c = respuesta4c;
    }

    public String getRespuesta4d() {
        return respuesta4d;
    }

    public void setRespuesta4d(String respuesta4d) {
        this.respuesta4d = respuesta4d;
    }

    public String getCorrecta1() {
        return correcta1;
    }

    public void setCorrecta1(String correcta1) {
        this.correcta1 = correcta1;
    }

    public String getCorrecta2() {
        return correcta2;
    }

    public void setCorrecta2(String correcta2) {
        this.correcta2 = correcta2;
    }

    public String getCorrecta3() {
        return correcta3;
    }

    public void setCorrecta3(String correcta3) {
        this.correcta3 = correcta3;
    }

    public String getCorrecta4() {
        return correcta4;
    }

    public void setCorrecta4(String correcta4) {
        this.correcta4 = correcta4;
    }

    public double getLatitud1() {
        return latitud1;
    }

    public void setLatitud1(double latitud1) {
        this.latitud1 = latitud1;
    }

    public double getLatitud2() {
        return latitud2;
    }

    public void setLatitud2(double latitud2) {
        this.latitud2 = latitud2;
    }

    public double getLatitud3() {
        return latitud3;
    }

    public void setLatitud3(double latitud3) {
        this.latitud3 = latitud3;
    }

    public double getLatitud4() {
        return latitud4;
    }

    public void setLatitud4(double latitud4) {
        this.latitud4 = latitud4;
    }

    public double getLogintud1() {
        return logintud1;
    }

    public void setLogintud1(double logintud1) {
        this.logintud1 = logintud1;
    }

    public double getLogintud2() {
        return logintud2;
    }

    public void setLogintud2(double logintud2) {
        this.logintud2 = logintud2;
    }

    public double getLogintud3() {
        return logintud3;
    }

    public void setLogintud3(double logintud3) {
        this.logintud3 = logintud3;
    }

    public double getLogintud4() {
        return logintud4;
    }

    public void setLogintud4(double logintud4) {
        this.logintud4 = logintud4;
    }
}
